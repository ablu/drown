SVG_FIGURES := $(wildcard figures/*.svg)
FIGURES := $(SVG_FIGURES:%.svg=%.png)

all: build/presentation.pdf build/paper.pdf $(FIGURES)

dirs:
	mkdir -p aux/ build/

build/%.pdf: %.tex $(FIGURES) dirs
	latexmk -output-directory=aux/ -interaction=nonstopmode -pdf $< && \
	cp aux/$(notdir $@)* build/

%.png: %.svg build-svg.sh
	./build-svg.sh $< $@

clean:
	git clean -dxf
