#!/usr/bin/bash

gcc -o check check.c -I /usr/include/ -lcrypto
gcc -o check_vulnerable check.c -I ../openssl-1.0.0t/install/include/ -L ../openssl-1.0.0t/install/lib  -lcrypto -ldl
